# Build instructions

* Download the FX3 SDK 1.2.3 [from here](http://www.cypress.com/documentation/software-and-drivers/ez-usb-fx3-sdk-archives)
* Unzip cy_fx3_sdk_v1.2.3_0.zip to get an "EZ-USB FX3 SDK" directory
* Download the FX3 SDK for Linux platforms [from here](https://www.cypress.com/documentation/software-and-drivers/ez-usb-fx3-software-development-kit)
* Untar FX3_SDK_1.3.4_Linux.tar.gz
* Untar ARM_GCC.tar.gz
* Launch the generate-firmware.sh script with usefull options (-h for help)

# Usage

You can use the UHD_IMAGES_DIR environment variable to set the firmware location path.
* First, copy the FPGA bitsream along to the generated firmware

`$ cp /usr/share/uhd/images/usrp_b210_fpga.bin <PATH_TO_GIT_REPOSITORY>/FX3-loopback-firmware-patch/build/uhd-release_003_010_003_000/firmware/fx3/b200/.`

* Set the UHD_IMAGES_DIR environment variable

`$ export UHD_IMAGES_DIR=<PATH_TO_GIT_REPOSITORY>/FX3-loopback-firmware-patch/build/uhd-release_003_010_003_000/firmware/fx3/b200`

* Use the uhd_usrp_probe application to write the images

`$ uhd_usrp_probe`