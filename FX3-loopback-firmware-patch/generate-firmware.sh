#!/bin/sh

abort()
{
  echo '\033[0;31mError: '$1'\033[0m'
  exit 1
}

display_step()
{
  echo '\033[0;33m* '$1'\033[0m'
}

display_usage()
{
  echo 'Usage: '`basename "$0"`' [SWITCHES]'
  echo ''
  echo '  -h                    Display this help'
  echo '  -s SDK_DIR            Set the SDK_DIR (should be EZ-USB SDK directory)'
  echo '  -t TOOLCHAIN_DIR      Set the toolchain directory (arm-2013.11 for instance)'
  echo '  -v TOOLCHAIN_VERSION  Set the toolchain version (4.8.1 for instance)'
}

# Parsing arguments
while getopts 'hs:t:v:' c
do
  case $c in
    h)
      display_usage
      exit 0 ;;
    s) SDK_DIR=$OPTARG;;
    t) TOOLCHAIN_DIR=$OPTARG;;
    v) TOOLCHAIN_VERSION=$OPTARG;;
  esac
done

if [ -z $SDK_DIR ]; then
  display_usage
  abort "please specify -s"
fi

if [ -z $TOOLCHAIN_DIR ]; then
  display_usage
  abort "please specify -t"
fi

if [ -z $TOOLCHAIN_VERSION ]; then
  display_usage
  abort "please specify -v"
fi

dir=$(pwd)

# Paths and constants
UHD_RELEASE_VERION=003_010_003_000

BUILD_DIR=$dir/build
UHD_SOURCE_DIR=$dir/build/uhd-release_$UHD_RELEASE_VERION

SDK_DIR_COMMON=$SDK_DIR/1.2/firmware/common
SDK_DIR_LPP_SOURCE=$SDK_DIR/1.2/firmware/lpp_source
SDK_DIR_U3P_FIRMWARE=$SDK_DIR/1.2/firmware/u3p_firmware

# Build
display_step "Creating build directory"
mkdir -p "$BUILD_DIR"
cd "$BUILD_DIR"

display_step "Getting sources"
wget -N https://github.com/EttusResearch/uhd/archive/release_$UHD_RELEASE_VERION.tar.gz || abort "failed to get sources"

display_step "Untar sources"
tar xvzf release_$UHD_RELEASE_VERION.tar.gz || abort "failed to untar sources"

display_step "Patching sources"
cd "$UHD_SOURCE_DIR"
patch -p1 < $dir/0001-B200-do-USB-loopback-using-DMAs.patch || abort "failed to patch sources"

display_step "Getting SDK subdirectories"

if [ ! -d "$SDK_DIR_COMMON" ]; then
  abort "$SDK_DIR_COMMON does not exists"
fi

if [ ! -d "$SDK_DIR_LPP_SOURCE" ]; then
  abort "$SDK_DIR_LPP_SOURCE does not exists"
fi

if [ ! -d "$SDK_DIR_U3P_FIRMWARE" ]; then
  abort "$SDK_DIR_U3P_FIRMWARE does not exists"
fi

cp -R "$SDK_DIR_COMMON" "$SDK_DIR_LPP_SOURCE" "$SDK_DIR_U3P_FIRMWARE" "$UHD_SOURCE_DIR/firmware/fx3" || abort "unable to copy files from SDK"

display_step "Patching the toolchain"
cd "$UHD_SOURCE_DIR/firmware/fx3/common"
patch -p2 < "$UHD_SOURCE_DIR/firmware/fx3/b200/fx3_mem_map.patch" || abort "failed to patch toolchain"

display_step "Building the firmware"
cd "$UHD_SOURCE_DIR/firmware/fx3/b200"

ARMGCC_INSTALL_PATH=$TOOLCHAIN_DIR ARMGCC_VERSION=$TOOLCHAIN_VERSION make || abort "unable to build firmware"

echo "\033[0;32mSuccessfuly generated $UHD_SOURCE_DIR/firmware/fx3/b200/usrp_b200_fw.hex\033[0m"
