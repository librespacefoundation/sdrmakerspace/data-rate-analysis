# SDR MakerSpace: Data rate analysis repository

## uhd-benchmark-app

This application enables a user to profile data transfert rate through UHD library.

## loopback-host-app

This application enables a user to benchmark USB transfer between a computer and the FX3 chip of the B210 board through libusb.

## FX3-loopback-firmware-patch

Set of scripts and files to generate a loopback firmware for FX3 chip. Please use this firmware with the loopback-host-app application.