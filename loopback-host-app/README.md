# loopback-host-app

Benchmark a B200 loopback firmware in order to get the USB throughput.
Please use this application with a patched version of the FX3 firmware.
To patch the FX3 firmware, please have a look at the FX3-loopback-firmware-patch folder of this git repository.

## Dependencies

* cmake
* libusb-1.0

## Building

* mkdir build
* cd build
* cmake ..
* make
