#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <libusb-1.0/libusb.h>

#include <time.h>
#include <pthread.h>

//#define CYPRESS_VID 0x04b4
//#define CYPRESS_PID 0x00f0

#define CYPRESS_VID 0x2500
#define CYPRESS_PID 0x0020

#define CYPRESS_EP_SEND 0x02
#define CYPRESS_EP_RECV 0x06

#define CYPRESS_IF_SEND 0x01
#define CYPRESS_IF_RECV 0x02

#define PACKET_COUNT 256
#define PACKET_SIZE 4*1024*1024

typedef struct {
	libusb_device_handle* dev;
	unsigned int packetSize;
	unsigned int totalSize;
	double* time;
	bool* success;
} ThreadArgs;

uint64_t gettimens()
{
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);

	return ((uint64_t)ts.tv_sec) * 1000000000ull + ((uint64_t)ts.tv_nsec);
}

void* txThreadFn(void* args)
{
	int ret, i, writtenBytes;
	char* buffer;
	uint64_t t1, t2;
	ThreadArgs* threadArgs = (ThreadArgs*)args;
	libusb_device_handle* dev = threadArgs->dev;

	*threadArgs->success = false;

	ret = libusb_claim_interface(dev, CYPRESS_IF_SEND);
	if (ret < 0) {
		fprintf(stderr, "libusb_claim_interface failed: %d\n", ret);
		libusb_close(dev);
		libusb_exit(NULL);
		return NULL;
	}

	buffer = malloc(PACKET_SIZE);
	t1 = gettimens();
	for (i = 0 ; i < PACKET_COUNT ; i++) {
		ret = libusb_bulk_transfer(dev, CYPRESS_EP_SEND, buffer, PACKET_SIZE, &writtenBytes, 2000);
		if ((ret != 0) || (writtenBytes != PACKET_SIZE)) {
			fprintf(stderr, "Error writting to USB: %d\n", ret);
			free(buffer);
			libusb_release_interface(dev, CYPRESS_IF_SEND);
			libusb_close(dev);
			libusb_exit(NULL);
			return NULL;
		}
	}
	t2 = gettimens();

	free(buffer);

	libusb_release_interface(dev, CYPRESS_IF_SEND);

	*threadArgs->time = ((double)(t2 - t1)) / 1e9;
	*threadArgs->success = true;

	return NULL;
}

void* rxThreadFn(void* args)
{
	int ret, i, readBytes;
	char* buffer;
	uint64_t t1, t2;
	ThreadArgs* threadArgs = (ThreadArgs*)args;
	libusb_device_handle* dev = threadArgs->dev;

	*threadArgs->success = false;

	ret = libusb_claim_interface(dev, CYPRESS_IF_RECV);

	buffer = malloc(PACKET_SIZE);
	t1 = gettimens();
	for (i = 0 ; i < PACKET_COUNT ; i++) {
		ret = libusb_bulk_transfer(dev, 0x80 | CYPRESS_EP_RECV, buffer, PACKET_SIZE, &readBytes, 2000);
		if ((ret != 0) || (readBytes != PACKET_SIZE)) {
			fprintf(stderr, "Error reading to USB: %d\n", ret);
			free(buffer);
			libusb_release_interface(dev, CYPRESS_IF_RECV);
			libusb_close(dev);
			libusb_exit(NULL);
			return NULL;
		}
	}
	t2 = gettimens();
	free(buffer);

	ret = libusb_release_interface(dev, CYPRESS_IF_RECV);
	if (ret < 0) {
		fprintf(stderr, "libusb_release_interface failed: %d\n", ret);
		libusb_close(dev);
		libusb_exit(NULL);
		return NULL;
	}

	*threadArgs->time = ((double)(t2 - t1)) / 1e9;
	*threadArgs->success = true;

	return NULL;
}

int main(int argc, char* argv[])
{
	bool txSuccess, rxSuccess;
	int ret, idx, writtenBytes, readBytes;
	double txTime, rxTime;
	libusb_device_handle* dev;
	pthread_t txThread, rxThread;
	ThreadArgs txThreadArgs, rxThreadArgs;

	ret = libusb_init(NULL);
	if (ret < 0) {
		fprintf(stderr, "libusb_init failed: %d\n", ret);
		return EXIT_FAILURE;
	}

	dev = libusb_open_device_with_vid_pid(NULL, CYPRESS_VID, CYPRESS_PID);
	if (!dev) {
		fprintf(stderr, "libusb_open_device_with_vid_pid failed: %d\n", ret);
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}

	txThreadArgs.dev = dev;
	rxThreadArgs.dev = dev;

	txThreadArgs.time = &txTime;
	rxThreadArgs.time = &rxTime;

	txSuccess = false;
	rxSuccess = false;

	txThreadArgs.success = &txSuccess;
	rxThreadArgs.success = &rxSuccess;

	if (pthread_create(&txThread, NULL, txThreadFn, &txThreadArgs)) {
		fprintf(stderr, "Unable to start TX thread\n");
		libusb_close(dev);
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}

	if (pthread_create(&rxThread, NULL, rxThreadFn, &rxThreadArgs)) {
		fprintf(stderr, "Unable to start RX thread\n");
		libusb_close(dev);
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}

	if (pthread_join(txThread, NULL)) {
		fprintf(stderr, "Unable to join TX thread\n");
		libusb_close(dev);
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}

	if (pthread_join(rxThread, NULL)) {
		fprintf(stderr, "Unable to join RX thread\n");
		libusb_close(dev);
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}

	double dataCountMb = ((double)(PACKET_SIZE * PACKET_COUNT)) / 1e6;

	if (*txThreadArgs.success) {
		printf("TX: %fMB/s\n", dataCountMb / *txThreadArgs.time);
	} else {
		fprintf(stderr, "TX thread failed\n");
	}

	if (*rxThreadArgs.success) {
		printf("RX: %fMB/s\n", dataCountMb / *rxThreadArgs.time);
	} else {
		fprintf(stderr, "RX thread failed\n");
	}

	libusb_close(dev);

	libusb_exit(NULL);

	return EXIT_SUCCESS;
}
