#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <libusb-1.0/libusb.h>

#include <time.h>
#include <pthread.h>

//#define CYPRESS_VID 0x04b4
//#define CYPRESS_PID 0x00f0

#define CYPRESS_VID 0x2500
#define CYPRESS_PID 0x0020

#define CYPRESS_EP_SEND 0x02
#define CYPRESS_EP_RECV 0x06

#define CYPRESS_IF_SEND 0x01
#define CYPRESS_IF_RECV 0x02

#define PACKET_COUNT 256
#define PACKET_SIZE 4*1024*1024

#define TX_TRANSFER_TIMEOUT 10000

struct TxTransferCbData {
	bool stop;
	bool success;
	unsigned int packetLeft;
};

uint64_t gettimens()
{
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);

	return ((uint64_t)ts.tv_sec) * 1000000000ull + ((uint64_t)ts.tv_nsec);
}

void txTransferCb(struct libusb_transfer* transfer)
{
	int ret;
	struct TxTransferCbData* data;

	data = (struct TxTransferCbData*)transfer->user_data;
	data->packetLeft--;

	if (data->packetLeft > 0) {
		ret = libusb_submit_transfer(transfer);
		if (ret != 0) {
			fprintf(stderr, "libusb_submit_transfer() failed: error %d\n", ret);
			data->success = false;
			data->stop = true;
		}
	} else {
		data->success = true;
		data->stop = true;
	}
}

bool transfer(libusb_device_handle* dev, double* txTime, double* rxTime)
{
	bool txFinish;
	int ret, i, readBytes;
	char* txBuffer;
	char* rxBuffer;
	uint64_t t1, t2;
	struct libusb_transfer* transfer;
	struct TxTransferCbData cbData;

	ret = libusb_claim_interface(dev, CYPRESS_IF_SEND);
	if (ret < 0) {
		fprintf(stderr, "libusb_claim_interface failed: %d\n", ret);
		goto exit_error;
	}

	ret = libusb_claim_interface(dev, CYPRESS_IF_RECV);
	if (ret < 0) {
		fprintf(stderr, "libusb_claim_interface failed: %d\n", ret);
		return false;
		goto exit_error_release_send_if;
	}

	txBuffer = malloc(PACKET_SIZE);
	rxBuffer = malloc(PACKET_SIZE);

	transfer = libusb_alloc_transfer(0);
	if (transfer == NULL) {
		fprintf(stderr, "libusb_alloc_transfer() failed\n");
		goto exit_error_free_buffers;
	}

	libusb_fill_bulk_transfer(transfer,
				  dev,
				  CYPRESS_EP_SEND,
				  txBuffer,
				  PACKET_SIZE,
				  txTransferCb,
				  &cbData,
				  TX_TRANSFER_TIMEOUT);

	cbData.packetLeft = PACKET_COUNT;

	t1 = gettimens();

	ret = libusb_submit_transfer(transfer);
	if (ret != 0) {
		fprintf(stderr, "libusb_submit_transfer() failed: error %d\n", ret);
		goto exit_error_free_transfer;
	}

	for (i = 0 ; i < PACKET_COUNT ; i++) {
		ret = libusb_bulk_transfer(dev,
					   0x80 | CYPRESS_EP_RECV,
					   rxBuffer,
					   PACKET_SIZE,
					   &readBytes,
					   2000);
		if ((ret != 0) || (readBytes != PACKET_SIZE)) {
			fprintf(stderr, "Error reading to USB: %d\n", ret);
			goto exit_error_free_transfer;
		}

	}

	while (!cbData.stop) {
		ret = libusb_handle_events(NULL);
		if (ret != LIBUSB_SUCCESS) {
			goto exit_error_cancel_transfer;
		}
	}

	t2 = gettimens();

	libusb_free_transfer(transfer);

	free(txBuffer);
	free(rxBuffer);

	ret = libusb_release_interface(dev, CYPRESS_IF_RECV);
	if (ret < 0) {
		fprintf(stderr, "libusb_release_interface failed: %d\n", ret);
		goto exit_error_release_send_if;
	}

	ret = libusb_release_interface(dev, CYPRESS_IF_SEND);
	if (ret < 0) {
		fprintf(stderr, "libusb_release_interface failed: %d\n", ret);
		goto exit_error;
	}

	if (!cbData.success) {
		fprintf(stderr, "Transfer failed\n");
		return false;
	}

	*txTime = ((double)(t2 - t1)) / 1000000000.0;
	*rxTime = *txTime;

	return true;

exit_error_cancel_transfer:
	libusb_cancel_transfer(transfer);

exit_error_free_transfer:
	libusb_free_transfer(transfer);

exit_error_free_buffers:
	free(txBuffer);
	free(rxBuffer);

	libusb_release_interface(dev, CYPRESS_IF_RECV);
exit_error_release_send_if:
	libusb_release_interface(dev, CYPRESS_IF_SEND);
exit_error:
	return false;
}

int main(int argc, char* argv[])
{
	int ret, idx, writtenBytes, readBytes;
	double txTime, rxTime;
	libusb_device_handle* dev;
	pthread_t txThread, rxThread;

	ret = libusb_init(NULL);
	if (ret < 0) {
		fprintf(stderr, "libusb_init failed: %d\n", ret);
		return EXIT_FAILURE;
	}

	dev = libusb_open_device_with_vid_pid(NULL, CYPRESS_VID, CYPRESS_PID);
	if (!dev) {
		fprintf(stderr, "libusb_open_device_with_vid_pid failed: %d\n", ret);
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}

	if (!transfer(dev, &txTime, &rxTime)) {
		fprintf(stderr, "Unable to start transfer\n");
		return EXIT_FAILURE;
	}

	double dataCountMb = ((double)(PACKET_SIZE * PACKET_COUNT)) / 1e6;

	printf("TX: %fMB/s\n", dataCountMb / txTime);
	printf("RX: %fMB/s\n", dataCountMb / rxTime);

	libusb_close(dev);

	libusb_exit(NULL);

	return EXIT_SUCCESS;
}
