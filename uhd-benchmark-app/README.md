# uhd-benchmark-app

uhd-benchmark-app is a utility tools designed to profile transfer rate on libuhd based compatible hardware.

## Supported hardware
* Ettus B210

## Dependencies

* libuhd

## Install

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
