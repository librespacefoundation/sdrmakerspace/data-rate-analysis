/*
 * Author: Kevin JOLY <kevin.joly@heig-vd.ch>
 *
 * This file is part of uhd-benchmark-app.
 *
 * uhd-benchmark-app is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uhd-benchmark-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uhd-benchmark-app.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <uhd/types/device_addr.hpp>
#include <uhd/device.hpp>

#include <iostream>
#include <thread>

#include <unistd.h>
#include <time.h>

#include "usrp-device.hpp"

void usage(const char* filename)
{
    std::cout << filename << " [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << "Benchmarking tool for libuhd based USRP platforms" << std::endl;
    std::cout << std::endl;
    std::cout << "    -h                Display this help." << std::endl;
    std::cout << "    -b COUNT          Set the sample count per buffer in byte." << std::endl;
    std::cout << "    -d DIR            Set direction (tx, rx or txrx)" << std::endl;
    std::cout << "    -l LOOP           Repeat LOOP times." << std::endl;
    std::cout << "    -o FREQ           Set oscillator frequency in Hz." << std::endl;
    std::cout << "    -r RATE           Set sampling rate in samples per seconds." << std::endl;
    std::cout << "    -s SIZE           Size to transfer (in samples)." << std::endl;
    std::cout << "    -t TYPE           Type to transfer (s16, f32 or f64)" << std::endl;
}

void print(double delta, size_t dataLen, size_t sampleSize)
{
    double dataLenMb = static_cast<double>(dataLen * sampleSize) / 1024.0 / 1024.0;
    double dataLenMsp = static_cast<double>(dataLen) / 1e6;
    double throughputMb = dataLenMb / delta;
    double throughputMsps = static_cast<double>(dataLen) / 1e6 / delta;

    std::cout <<  dataLenMsp << "M samples (" << dataLenMb << "MB) transfered in "
              << delta << "s [" << throughputMsps << " Msps ("<< throughputMb << "MB/s)]" << std::endl;
}

template<typename samp_type>
void txThreadFn(USRPDevice* device, const std::complex<samp_type>* buffer, size_t sz, double &delta, bool &result)
{
    struct timespec ts1, ts2;

    clock_gettime(CLOCK_MONOTONIC, &ts1);
    result = device->send(buffer, static_cast<size_t>(sz));
    clock_gettime(CLOCK_MONOTONIC, &ts2);

    double t1 = static_cast<double>(ts1.tv_sec) + static_cast<double>(ts1.tv_nsec)/1e9;
    double t2 = static_cast<double>(ts2.tv_sec) + static_cast<double>(ts2.tv_nsec)/1e9;
    delta = t2 - t1;
}

template<typename samp_type>
void rxThreadFn(USRPDevice* device, std::complex<samp_type>* buffer, size_t sz, double &delta, bool &result)
{
    struct timespec ts1, ts2;

    clock_gettime(CLOCK_MONOTONIC, &ts1);
    result = device->receive(buffer, sz);
    clock_gettime(CLOCK_MONOTONIC, &ts2);

    double t1 = static_cast<double>(ts1.tv_sec) + static_cast<double>(ts1.tv_nsec)/1e9;
    double t2 = static_cast<double>(ts2.tv_sec) + static_cast<double>(ts2.tv_nsec)/1e9;
    delta = t2 - t1;
}

template<typename samp_type>
bool profile(USRPDevice& device, size_t sampleSizeToTransfert, bool enableTx, bool enableRx)
{
    double txDelta, rxDelta;
    bool txResult, rxResult;

    std::complex<samp_type>* txBuffer = new std::complex<samp_type>[sampleSizeToTransfert];
    std::complex<samp_type>* rxBuffer = new std::complex<samp_type>[sampleSizeToTransfert];

    std::thread txThread;

    if (enableTx) {
        txThread = std::thread(txThreadFn<samp_type>,
                               &device,
                               txBuffer,
                               sampleSizeToTransfert,
                               std::ref(txDelta),
                               std::ref(txResult));
    }

    std::thread rxThread;
    if (enableRx) {
        rxThread = std::thread(rxThreadFn<samp_type>,
                               &device,
                               rxBuffer,
                               sampleSizeToTransfert,
                               std::ref(rxDelta),
                               std::ref(rxResult));
    }

    if (enableTx) {
        txThread.join();
    }

    if (enableRx) {
        rxThread.join();
    }

    /* XXX */
    delete[] txBuffer;
    delete[] rxBuffer;

    if (enableTx && !txResult) {
        std::cerr << "Send error" << std::endl;
        return false;
    }

    if (enableRx && !rxResult) {
        std::cerr << "Receive error" << std::endl;
        return false;
    }

    if (enableTx) {
        std::cout << "TX:" << std::endl;
        print(txDelta, sampleSizeToTransfert, sizeof(std::complex<samp_type>));
    }

    if (enableRx) {
        std::cout << "RX:" << std::endl;
        print(rxDelta, sampleSizeToTransfert, sizeof(std::complex<samp_type>));
    }

    return true;
}

int UHD_SAFE_MAIN(int argc, char* argv[])
{
    bool txEnable = true, rxEnable = true;
    USRPDevice::CPUFormat_t sampleType = USRPDevice::FLOAT32;
    unsigned int samplingRateSps = 400000;
    unsigned int oscillatorFrequencyHz = 2480000000;
    size_t sampleSizeToTransfert = 1e6;
    size_t samplesPerBuffer = 4096;
    unsigned int loop = 1;

    if (!uhd::set_thread_priority_safe()) {
        std::cerr << "Unable to set thread priority" << std::endl;
        return EXIT_FAILURE;
    }

    int opt;
    while ((opt = getopt(argc, argv, "hb:d:l:o:r:s:t:")) != -1) {
        switch (opt) {
            case 'h':
                usage(argv[0]);
                return 1;
                break;

            case 'b':
                samplesPerBuffer = atoi(optarg);
                break;

            case 'd':
                if (!strcmp(optarg, "tx")) {
                    txEnable = true;
                    rxEnable = false;
                } else if (!strcmp(optarg, "rx")) {
                    txEnable = false;
                    rxEnable = true;
                } else if (!strcmp(optarg, "txrx")) {
                    txEnable = true;
                    rxEnable = true;
                } else {
                    std::cerr << "Bad sample type" << std::endl;
                    usage(argv[0]);
                    return -1;
                }
                break;

            case 'l':
                loop = atoi(optarg);
                break;

            case 'o':
                oscillatorFrequencyHz = atoi(optarg);
                break;

            case 'r':
                samplingRateSps = atoi(optarg);
                break;

            case 's':
                sampleSizeToTransfert = atoi(optarg);
                break;

            case 't':

                if (!strcmp(optarg, "s16")) {
                    sampleType = USRPDevice::SIGNED16;
                } else if (!strcmp(optarg, "f32")) {
                    sampleType = USRPDevice::FLOAT32;
                } else if (!strcmp(optarg, "f64")) {
                    sampleType = USRPDevice::FLOAT64;
                } else {
                    std::cerr << "Bad sample type" << std::endl;
                    usage(argv[0]);
                    return -1;
                }

                break;

            default:
                abort();
        }
    }

    USRPDevice device(samplingRateSps, oscillatorFrequencyHz, sampleType);
    if (!device.initialize()) {
        std::cerr << "Failed to initialize device" << std::endl;
        return EXIT_FAILURE;
    }

    device.setSamplesPerBuffer(samplesPerBuffer);

    unsigned int succeed = 0;
    for (unsigned int i = 0 ; i < loop ; i++) {
        bool ret = false;
        switch(sampleType) {
            case USRPDevice::SIGNED16:
                ret = profile<short>(device, sampleSizeToTransfert, txEnable, rxEnable);
                break;
            case USRPDevice::FLOAT32:
                ret = profile<float>(device, sampleSizeToTransfert, txEnable, rxEnable);
                break;
            case USRPDevice::FLOAT64:
                ret = profile<double>(device, sampleSizeToTransfert, txEnable, rxEnable);
                break;
        };

        if (ret) {
            succeed++;
        }
    }

    std::cout << succeed << "/" << loop << " succeed" << std::endl;

    return succeed == loop ? EXIT_SUCCESS : EXIT_FAILURE;
}
