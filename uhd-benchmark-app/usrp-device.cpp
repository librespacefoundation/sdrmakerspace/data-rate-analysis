/*
 * Author: Kevin JOLY <kevin.joly@heig-vd.ch>
 *
 * This file is part of uhd-benchmark-app.
 *
 * uhd-benchmark-app is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uhd-benchmark-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uhd-benchmark-app.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "usrp-device.hpp"

const uhd::usrp::subdev_spec_t USRPDevice::TxSubdevSpec("A:A");
const uhd::usrp::subdev_spec_t USRPDevice::RxSubdevSpec("A:A");
const std::string USRPDevice::ClockSource("internal");
const std::string USRPDevice::TimeSource("internal");
const double USRPDevice::LOOffset = 0.0;
const double USRPDevice::Gain = 0.0;
const double USRPDevice::Bandwidth = 4 * 1.e6;
const std::string USRPDevice::TxAntenna = "TX/RX";
const std::string USRPDevice::RxAntenna = "TX/RX";
const std::string USRPDevice::CPUFormatSigned16Str = "sc16";
const std::string USRPDevice::CPUFormatFloat32Str = "fc32";
const std::string USRPDevice::CPUFormatFloat64Str = "fc64";
const std::string USRPDevice::WireFormat = "sc16";
const size_t USRPDevice::DefaultSamplesPerBuffer = 4092;

USRPDevice::USRPDevice(double sampleRate,
                       double centerFrequencyHz,
                       CPUFormat_t cpuFormat,
                       std::string address)
:_sampleRate(sampleRate),
 _centerFrequencyHz(centerFrequencyHz),
 _cpuFormat(cpuFormat),
 _address(address),
 _samplesPerBuffer(DefaultSamplesPerBuffer)
{
}

bool USRPDevice::initialize()
{
    uhd::device_addr_t args(_address);
    uhd::device_addrs_t device_addrs = uhd::device::find(args, uhd::device::USRP);

    if (device_addrs.empty()) {
        std::cerr << "No device found" << std::endl;
        return false;
    }

    _usrp = uhd::usrp::multi_usrp::make(device_addrs[0]);

    // TODO if ClockSource != internal, must wait for lock (usrp->get_mboard_sensor("ref_locked")
    _usrp->set_clock_source(ClockSource);

    // Set TX subdev
    _usrp->set_tx_subdev_spec(TxSubdevSpec);
    _usrp->set_rx_subdev_spec(RxSubdevSpec);

    // Set time source
    //_usrp->set_time_source(TimeSource);

    // Set the rate
    _usrp->set_tx_rate(_sampleRate);
    _usrp->set_rx_rate(_sampleRate);

    // Set the center frequency
    uhd::tune_request_t txTuneReq = uhd::tune_request_t(_centerFrequencyHz, LOOffset);
    _usrp->set_tx_freq(txTuneReq);

    uhd::tune_request_t rxTuneReq(_centerFrequencyHz);
    _usrp->set_rx_freq(rxTuneReq);

    // Set the gain
    _usrp->set_tx_gain(Gain);
    _usrp->set_rx_gain(Gain);

    // Set the bandwidth
    _usrp->set_tx_bandwidth(Bandwidth);
    _usrp->set_rx_bandwidth(Bandwidth);

    // Set the antenna
    _usrp->set_tx_antenna(TxAntenna);
    _usrp->set_rx_antenna(RxAntenna);

    // Set up time
    sleep(1);

    // Checking clocks
    std::vector<std::string> txSensorNames = _usrp->get_tx_sensor_names(0);

    if (std::find(txSensorNames.begin(), txSensorNames.end(), "lo_locked") != txSensorNames.end()) {
        uhd::sensor_value_t loLocked = _usrp->get_tx_sensor("lo_locked", 0);
        if (!loLocked.to_bool()) {
            std::cerr << "LO not locked" << std::endl;
            return false;
        }
    }

    std::vector<std::string> rxSensorNames = _usrp->get_rx_sensor_names(0);

    if (std::find(rxSensorNames.begin(), rxSensorNames.end(), "lo_locked") != rxSensorNames.end()) {
        uhd::sensor_value_t loLocked = _usrp->get_rx_sensor("lo_locked", 0);
        if (!loLocked.to_bool()) {
            std::cerr << "LO not locked" << std::endl;
            return false;
        }
    }

    //std:.vector<std::string> boardSensorNames = usrp->get_mboard_sensor_names(0);
    // TODO should be used in case of mimo or external reference source

    /* Create streams */
    std::string cpuFormatStr;

    switch (_cpuFormat) {
        case SIGNED16:
            cpuFormatStr = CPUFormatSigned16Str;
            break;
        case FLOAT32:
            cpuFormatStr = CPUFormatFloat32Str;
            break;
        case FLOAT64:
            cpuFormatStr = CPUFormatFloat64Str;
            break;
    };

    uhd::stream_args_t streamArgs(cpuFormatStr, WireFormat);
    std::vector<size_t> channelNums;
    streamArgs.channels.push_back(0);
    _txStream = _usrp->get_tx_stream(streamArgs);

    _rxStream = _usrp->get_rx_stream(streamArgs);

    return true;
}

void USRPDevice::setSamplesPerBuffer(size_t samplesPerBuffer)
{
    _samplesPerBuffer = samplesPerBuffer;
}

bool USRPDevice::send(const std::complex<short>* buffer, size_t sz)
{
    return send<std::complex<short> >(static_cast<const void*>(buffer), sz);
}

bool USRPDevice::send(const std::complex<float>* buffer, size_t sz)
{
    return send<std::complex<float> >(static_cast<const void*>(buffer), sz);
}

bool USRPDevice::send(const std::complex<double>* buffer, size_t sz)
{
    return send<std::complex<double> >(static_cast<const void*>(buffer), sz);
}

template<typename samp_type>
bool USRPDevice::send(const void* buffer, size_t sz)
{
    uhd::tx_metadata_t md;
    md.start_of_burst = false;
    md.end_of_burst   = false;

    const samp_type* d = static_cast<const samp_type*>(buffer);

    // Fragment the buffer to send into _samplesPerBuffer length
    size_t packetsToSend = sz / _samplesPerBuffer;

    for (size_t i = 0 ; i < packetsToSend ; i++) {

        size_t ret = _txStream->send(d, _samplesPerBuffer, md);
        if (ret != _samplesPerBuffer) {
            std::cerr << "Send failed (" << ret << "/" << _samplesPerBuffer << ")" << std::endl;
            return false;
        }

        d += _samplesPerBuffer;
    }

    // Send leftovers
    size_t leftovers = sz % _samplesPerBuffer;
    size_t ret = _txStream->send(d, leftovers, md);
    if (ret != leftovers) {
        std::cerr << "Send failed (" << ret << "/" << leftovers << ")" << std::endl;
        return false;
    }

    // Send end empty end of burst packet
    md.end_of_burst = true;
    ret = _txStream->send("", 0, md);
    if (ret != 0) {
        std::cerr << "Send eob failed (" << ret << "/" << 0 << ")" << std::endl;
        return false;
    }

    // Wait for burst ack async message to come
    uhd::async_metadata_t async_md;
    if (!_txStream->recv_async_msg(async_md)) {
        std::cerr << "recv_async_msg timed out" << std::endl;
        return false;
    }

    return (async_md.event_code == uhd::async_metadata_t::EVENT_CODE_BURST_ACK);
}

bool USRPDevice::receive(std::complex<short>* buffer, size_t sz)
{
    return receive<std::complex<short> >(static_cast<void*>(buffer), sz);
}

bool USRPDevice::receive(std::complex<float>* buffer, size_t sz)
{
    return receive<std::complex<float> >(static_cast<void*>(buffer), sz);
}

bool USRPDevice::receive(std::complex<double>* buffer, size_t sz)
{
    return receive<std::complex<double> >(static_cast<void*>(buffer), sz);
}

template<typename samp_type>
bool USRPDevice::receive(void* buffer, size_t sz)
{
    uhd::rx_metadata_t md;

    uhd::stream_cmd_t streamCmd(uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE);
    streamCmd.num_samps = sz;
    streamCmd.stream_now = true;
    streamCmd.time_spec = uhd::time_spec_t();
    _rxStream->issue_stream_cmd(streamCmd);

    size_t numRxSamps = _rxStream->recv(buffer, sz, md);

    if (md.error_code != uhd::rx_metadata_t::ERROR_CODE_NONE) {
        switch (md.error_code) {
            case uhd::rx_metadata_t::ERROR_CODE_TIMEOUT:
                std::cerr << "Error: Timeout" << std::endl;
                return false;
                break;

            case uhd::rx_metadata_t::ERROR_CODE_OVERFLOW:
                std::cerr << "Error: overflow" << std::endl;
                return false;
                break;

            case uhd::rx_metadata_t::ERROR_CODE_NONE:
                std::cerr << "Error: " << md.strerror() << std::endl;
                return false;
                break;

            case uhd::rx_metadata_t::ERROR_CODE_LATE_COMMAND:
                std::cerr << "Error: late command" << std::endl;
                return false;
                break;

            case uhd::rx_metadata_t::ERROR_CODE_BROKEN_CHAIN:
                std::cerr << "Error: broken chain" << std::endl;
                return false;
                break;

            case uhd::rx_metadata_t::ERROR_CODE_ALIGNMENT:
                std::cerr << "Error: alignment" << std::endl;
                return false;
                break;

            case uhd::rx_metadata_t::ERROR_CODE_BAD_PACKET:
                std::cerr << "Error: bad packet" << std::endl;
                return false;
                break;

            default:
                std::cerr << "Unknown error" << std::endl;
                return false;
                break;
        }

    }

    return  (numRxSamps == sz);
}
