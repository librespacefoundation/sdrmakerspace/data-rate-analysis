/*
 * Author: Kevin JOLY <kevin.joly@heig-vd.ch>
 *
 * This file is part of uhd-benchmark-app.
 *
 * uhd-benchmark-app is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uhd-benchmark-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uhd-benchmark-app.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef USRP_DEVICE_HPP
#define USRP_DEVICE_HPP

#include <uhd/usrp/multi_usrp.hpp>

class USRPDevice {
public:
    enum CPUFormat_t{
        SIGNED16,
        FLOAT32,
        FLOAT64
    };

public:

    /**
     * \param sampleRate Sampling rate in samples per seconds.
     * \param centerFrequencyHz Center frequency of the oscillator in Hz.
     * \param cpuFormat Sample format (short, float or double). Please call the send function accordingly.
     * \param address Address of the USRP device. "" = any.
     */
    USRPDevice(double sampleRate, double centerFrequencyHz, CPUFormat_t cpuFormat, std::string address = "");

    /**
     * Must be called prior send but after *set* functions.
     */
    bool initialize();

    /**
     * Set the samples per buffer length. Please call this function before \ref initialize().
     */
    void setSamplesPerBuffer(size_t samplesPerBuffer);

    bool send(const std::complex<short>* buffer, size_t sz);
    bool send(const std::complex<float>* buffer, size_t sz);
    bool send(const std::complex<double>* buffer, size_t sz);

    bool receive(std::complex<short>* buffer, size_t sz);
    bool receive(std::complex<float>* buffer, size_t sz);
    bool receive(std::complex<double>* buffer, size_t sz);

private:
    template<typename samp_type>
    bool send(const void* buffer, size_t sz);

    template<typename samp_type>
    bool receive(void* buffer, size_t sz);

private:
    uhd::usrp::multi_usrp::sptr _usrp;
    uhd::tx_streamer::sptr _txStream;
    uhd::rx_streamer::sptr _rxStream;
    double _sampleRate;
    double _centerFrequencyHz;
    CPUFormat_t _cpuFormat;
    std::string _address;
    size_t _samplesPerBuffer;

    static const uhd::usrp::subdev_spec_t TxSubdevSpec;
    static const uhd::usrp::subdev_spec_t RxSubdevSpec;
    static const std::string ClockSource;
    static const std::string TimeSource;
    static const double LOOffset;
    static const double Gain;
    static const double Bandwidth;
    static const std::string TxAntenna;
    static const std::string RxAntenna;
    static const std::string CPUFormatSigned16Str;
    static const std::string CPUFormatFloat32Str;
    static const std::string CPUFormatFloat64Str;
    static const std::string WireFormat;
    static const size_t DefaultSamplesPerBuffer;

};
#endif /* USRP_DEVICE_HPP */
